package retrier_test

import (
	"context"
	"fmt"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/gopkgz/transient"
	"gitlab.com/gopkgz/transient/retrier"
)

type counter struct {
	retries atomic.Int32
}

func (r *counter) Do(fn func() error) error {
	r.retries.Add(1)
	return fn()
}

type failNTimes struct {
	retries atomic.Int32
	n       int
	err     error
}

func (f *failNTimes) Run() error {
	f.retries.Add(1)

	if f.retries.Load() <= int32(f.n) {
		return f.err
	}
	return nil
}

func TestRetries(t *testing.T) {
	someErr := fmt.Errorf("some error")
	transientErr := transient.Error{Err: someErr}

	failer1 := &failNTimes{n: 1, err: transientErr}
	failer3 := &failNTimes{n: 3, err: transientErr}
	failer100 := &failNTimes{n: 100, err: transientErr}

	testCases := []struct {
		name             string
		testFn           func() error
		expectedRunCount int
		expectedResult   error
	}{
		{
			name:             "noop",
			testFn:           func() error { return nil },
			expectedRunCount: 1,
			expectedResult:   nil,
		},
		{
			name:             "fail once",
			testFn:           failer1.Run,
			expectedRunCount: 2,
			expectedResult:   nil,
		},
		{
			name:             "fail three times",
			testFn:           failer3.Run,
			expectedRunCount: 4,
			expectedResult:   nil,
		},
		{
			name:             "fail max times",
			testFn:           failer100.Run,
			expectedRunCount: 5,
			expectedResult:   transientErr,
		},
		{
			name: "non-transient error",
			testFn: func() error {
				return someErr
			},
			expectedRunCount: 1,
			expectedResult:   someErr,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			c := &counter{}
			r := retrier.Retrier{
				Factor: 1,
				Delay:  10 * time.Millisecond,
				Max:    5,
			}
			actualResult := r.Do(context.Background(), func() error {
				return c.Do(tc.testFn)
			})
			if actualResult != tc.expectedResult {
				t.Errorf("expected result %v, got %v", tc.expectedResult, actualResult)
			}
			retryCount := int(c.retries.Load())
			if retryCount != tc.expectedRunCount {
				t.Errorf("expected retry count %d, got %d", tc.expectedRunCount, retryCount)
			}
		})
	}
}

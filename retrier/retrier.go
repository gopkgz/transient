package retrier

import (
	"context"
	"fmt"
	"math"
	"math/rand"
	"sync"
	"time"

	"gitlab.com/gopkgz/transient"
)

const (
	defaultJitterPercent = 0.1
	defaultFactor        = 1.5
	defaultRetryCount    = 5
	defaultInitialDelay  = 100 * time.Millisecond
	noJitterUnder        = 500 * time.Millisecond
)

// Retrier retries transient errors with exponential backoff.
type Retrier struct {
	Max           int
	Delay         time.Duration
	MaxDelay      time.Duration
	Factor        float64
	JitterPercent float64
	ErrorChecker  func(error) bool
	once          sync.Once
	rnd           *rand.Rand
}

func (r *Retrier) delay(i int) time.Duration {
	maxDelay := r.Delay * time.Duration(math.Pow(r.Factor, float64(i)))
	if r.MaxDelay > 0 && maxDelay > r.MaxDelay {
		maxDelay = r.MaxDelay
	}

	if maxDelay < noJitterUnder || r.JitterPercent == 0 {
		return maxDelay
	}

	// random jitter up to jitterPercent
	randMax := float64(maxDelay.Milliseconds())
	randMin := randMax - (float64(maxDelay.Milliseconds()) * r.JitterPercent)
	return time.Duration(randFloat(randMin, randMax)) * time.Millisecond
}

func (r *Retrier) Do(ctx context.Context, fn func() error) error {
	r.once.Do(func() {
		r.rnd = rand.New(rand.NewSource(int64(time.Now().Nanosecond())))

		if r.Max == 0 {
			r.Max = defaultRetryCount
		}
		if r.Delay == 0 {
			r.Delay = defaultInitialDelay
		}
		if r.Factor == 0 {
			r.Factor = defaultFactor
		}
		if r.JitterPercent == 0 {
			r.JitterPercent = defaultJitterPercent
		}
		if r.ErrorChecker == nil {
			r.ErrorChecker = transient.IsTransientError
		}
	})

	if r.Factor < 1 {
		return fmt.Errorf("factor cannot be less than 1")
	}

	var err error
	for i := 0; i < r.Max; i++ {
		if err = fn(); err == nil {
			return nil
		}

		if !r.ErrorChecker(err) {
			return err
		}

		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-time.After(r.delay(i)):
		}
	}

	return err
}

func randFloat(min, max float64) float64 {
	return min + rand.Float64()*(max-min)
}

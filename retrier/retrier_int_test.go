package retrier

import (
	"context"
	"testing"
	"time"
)

func TestNewRetrier(t *testing.T) {
	r := Retrier{}
	_ = r.Do(context.Background(), func() error { return nil })

	if r.Max != defaultRetryCount {
		t.Errorf("expected Max to be %d, got %d", defaultRetryCount, r.Max)
	}
	if r.Factor != defaultFactor {
		t.Errorf("expected Factor to be %f, got %f", defaultFactor, r.Factor)
	}
	if r.Delay != defaultInitialDelay {
		t.Errorf("expected Delay to be %s, got %s", defaultInitialDelay, r.Delay)
	}
	if r.JitterPercent != defaultJitterPercent {
		t.Errorf("expected JitterPercent to be %f, got %f", defaultJitterPercent, r.JitterPercent)
	}
	if r.ErrorChecker == nil {
		t.Error("expected ErrorChecker to be not nil")
	}
}

func TestDelay(t *testing.T) {
	testCases := []struct {
		name             string
		i                int
		factor           float64
		delay            time.Duration
		maxDelay         time.Duration
		jitterPercent    float64
		expectedDelay    time.Duration
		expectedDelayMin time.Duration
		expectedDelayMax time.Duration
	}{
		{
			name:             "no jitter under threshold",
			i:                1,
			factor:           1,
			delay:            noJitterUnder - 1*time.Millisecond,
			jitterPercent:    defaultJitterPercent,
			expectedDelay:    noJitterUnder - 1*time.Millisecond,
			expectedDelayMin: 0,
			expectedDelayMax: 0,
		},
		{
			name:             "no jitter if disabled",
			i:                1,
			factor:           1,
			delay:            10 * time.Second,
			jitterPercent:    0,
			expectedDelay:    10 * time.Second,
			expectedDelayMin: 0,
			expectedDelayMax: 0,
		},
		{
			name:             "jitter at threshold",
			i:                1,
			factor:           1,
			delay:            noJitterUnder,
			jitterPercent:    defaultJitterPercent,
			expectedDelay:    0,
			expectedDelayMin: delayMin(noJitterUnder, defaultJitterPercent),
			expectedDelayMax: noJitterUnder,
		},
		{
			name:             "first iteration delay",
			i:                0,
			factor:           1000,
			delay:            100 * time.Millisecond,
			jitterPercent:    defaultJitterPercent,
			expectedDelay:    100 * time.Millisecond,
			expectedDelayMin: 0,
			expectedDelayMax: 0,
		},
		{
			name:             "second iteration delay",
			i:                1,
			factor:           2,
			delay:            10 * time.Millisecond,
			jitterPercent:    defaultJitterPercent,
			expectedDelay:    20 * time.Millisecond,
			expectedDelayMin: 0,
			expectedDelayMax: 0,
		},
		{
			name:             "third iteration delay",
			i:                2,
			factor:           2,
			delay:            10 * time.Millisecond,
			jitterPercent:    defaultJitterPercent,
			expectedDelay:    40 * time.Millisecond,
			expectedDelayMin: 0,
			expectedDelayMax: 0,
		},
		{
			name:             "factor=1",
			i:                20,
			factor:           1,
			delay:            10 * time.Millisecond,
			jitterPercent:    defaultJitterPercent,
			expectedDelay:    10 * time.Millisecond,
			expectedDelayMin: 0,
			expectedDelayMax: 0,
		},
		{
			name:             "max delay",
			i:                20,
			factor:           2,
			delay:            100 * time.Millisecond,
			maxDelay:         200 * time.Millisecond,
			jitterPercent:    defaultJitterPercent,
			expectedDelay:    200 * time.Millisecond,
			expectedDelayMin: 0,
			expectedDelayMax: 0,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			r := Retrier{
				Factor:        tc.factor,
				Delay:         tc.delay,
				MaxDelay:      tc.maxDelay,
				JitterPercent: tc.jitterPercent,
			}
			actualDelay := r.delay(tc.i)
			if tc.expectedDelay != 0 {
				if actualDelay != tc.expectedDelay {
					t.Errorf("expected delay %v, got %v", tc.expectedDelay, actualDelay)
				}
				return
			}
			if actualDelay < tc.expectedDelayMin || actualDelay > tc.expectedDelayMax {
				t.Errorf("expected delay to be in [%s, %s], got %s", tc.expectedDelayMin, tc.expectedDelayMax, actualDelay)
			}
		})
	}
}

func delayMin(tm time.Duration, jitterPercent float64) time.Duration {
	return tm - time.Duration(jitterPercent*float64(tm.Milliseconds()))*time.Millisecond
}

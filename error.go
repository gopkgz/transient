package transient

import (
	"errors"
	"fmt"
)

type Error struct {
	Err error
}

func (e Error) Transient() bool {
	return true
}

func (e Error) Error() string {
	return fmt.Sprintf("Transient error %s", e.Err)
}

func (e Error) Unwrap() error {
	return e.Err
}

func IsTransientError(err error) bool {
	var tErr Error
	return err != nil && errors.As(err, &tErr) && tErr.Transient()
}

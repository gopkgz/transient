package requester_test

import (
	"fmt"
	"net"
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/gopkgz/transient"
	"gitlab.com/gopkgz/transient/requester"
)

func TestRequesterHTTPError(t *testing.T) {
	testCases := []struct {
		name                   string
		client                 requester.Doer
		expectedRetryableError bool
	}{
		{"nil", nil, false},
		{
			"net.DNSError",
			&httpErrorClient{Err: &net.OpError{Err: &net.DNSError{IsTimeout: true}}},
			true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			r := requester.New(tc.client, nil, nil)
			_, err := r.Do(&http.Request{URL: urlMustParse("http://example.com")})
			if tc.expectedRetryableError && err == nil {
				t.Error("expected error, got nil")
			}

			if transient.IsTransientError(err) != tc.expectedRetryableError {
				t.Errorf("expected retryable %t, got %t", tc.expectedRetryableError, transient.IsTransientError(err))
			}
		})
	}
}

func TestRequesterRetryableHTTPStatus(t *testing.T) {
	testCases := []struct {
		name              string
		client            requester.Doer
		expectedRetryable bool
	}{
		{"nil", nil, false},
		{
			name: "non-retryable",
			client: &httpStatusErrorClient{
				StatusCode: http.StatusMethodNotAllowed,
			},
			expectedRetryable: false,
		},
	}

	for _, status := range []int{
		http.StatusTooManyRequests,
		http.StatusBadRequest,
		http.StatusUnauthorized,
		http.StatusForbidden,
		http.StatusNotFound,
		http.StatusRequestTimeout,
		http.StatusTooEarly,
		http.StatusInternalServerError,
		http.StatusBadGateway,
		http.StatusServiceUnavailable,
		http.StatusGatewayTimeout,
	} {
		testCases = append(testCases, struct {
			name              string
			client            requester.Doer
			expectedRetryable bool
		}{
			name:              fmt.Sprintf("HTTP %d", status),
			client:            &httpStatusErrorClient{StatusCode: status},
			expectedRetryable: true,
		})
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			r := requester.New(tc.client, nil, nil)
			_, err := r.Do(&http.Request{URL: urlMustParse("http://example.com")})
			if err != nil {
				if transient.IsTransientError(err) != tc.expectedRetryable {
					t.Errorf("expected transient error, got %v", err)
				}
			}
		})
	}
}

func TestRequesterHTTPOK(t *testing.T) {
	okStatuses := []int{
		http.StatusOK,
		http.StatusCreated,
		http.StatusAccepted,
	}

	testCases := []struct {
		name      string
		client    requester.Doer
		expectErr bool
	}{
		{"nil", nil, true},
		{"HTTP 400", &httpStatusErrorClient{StatusCode: http.StatusBadRequest}, true},
	}

	for _, status := range okStatuses {
		testCases = append(testCases, struct {
			name      string
			client    requester.Doer
			expectErr bool
		}{
			name:      fmt.Sprintf("HTTP %d", status),
			client:    &httpStatusErrorClient{StatusCode: status},
			expectErr: false,
		})
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			r := requester.New(tc.client, okStatuses, nil)
			_, err := r.Do(&http.Request{URL: urlMustParse("http://example.com")})
			if err != nil && !tc.expectErr {
				t.Errorf("expected no error, got %v", err)
			}
		})
	}
}

type httpStatusErrorClient struct {
	StatusCode int
}

func (c *httpStatusErrorClient) Do(req *http.Request) (*http.Response, error) {
	return &http.Response{
		StatusCode: c.StatusCode,
		Body:       http.NoBody,
	}, nil
}

type httpErrorClient struct {
	Err error
}

func (c *httpErrorClient) Do(req *http.Request) (*http.Response, error) {
	return nil, c.Err
}

func urlMustParse(u string) *url.URL {
	parsed, err := url.Parse(u)
	if err != nil {
		panic(err)
	}
	return parsed
}

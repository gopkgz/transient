package requester

import "fmt"

type HTTPRequestError struct {
	Err  error
	Path string
}

func (e HTTPRequestError) Error() string {
	return fmt.Sprintf("HTTP request error: %s, URL %s", e.Err, e.Path)
}

func (e HTTPRequestError) Unwrap() error {
	return e.Err
}

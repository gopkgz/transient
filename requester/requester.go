package requester

import (
	"net/http"
	"os"

	"gitlab.com/gopkgz/transient"
)

type Requester struct {
	Client            Doer
	OKStatuses        map[int]struct{}
	TransientStatuses map[int]struct{}
}

func (r *Requester) Do(req *http.Request) (*http.Response, error) {
	res, err := r.Client.Do(req)
	if err != nil {
		if os.IsTimeout(err) {
			return nil, transient.Error{Err: err}
		}

		return nil, HTTPRequestError{Err: err, Path: req.URL.String()}
	}

	if _, ok := r.OKStatuses[res.StatusCode]; ok {
		return res, nil
	}

	defer res.Body.Close()
	if _, ok := r.TransientStatuses[res.StatusCode]; ok {
		return nil, transient.Error{Err: HTTPStatusError{req.URL.String(), res.StatusCode, res.Status}}
	}

	return nil, HTTPStatusError{req.URL.String(), res.StatusCode, res.Status}
}

func New(httpClient Doer, okStatusCodes []int, transientStatusCodes []int) *Requester {
	if okStatusCodes == nil {
		okStatusCodes = []int{http.StatusOK}
	}

	if transientStatusCodes == nil {
		transientStatusCodes = TransientStatuses()
	}

	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	return &Requester{
		Client:            httpClient,
		OKStatuses:        sliceToMap(okStatusCodes),
		TransientStatuses: sliceToMap(transientStatusCodes),
	}
}

func TransientStatuses() []int {
	return []int{
		http.StatusTooManyRequests,
		http.StatusBadRequest,
		http.StatusUnauthorized,
		http.StatusForbidden,
		http.StatusNotFound,
		http.StatusRequestTimeout,
		http.StatusTooEarly,
		http.StatusInternalServerError,
		http.StatusBadGateway,
		http.StatusServiceUnavailable,
		http.StatusGatewayTimeout,
	}
}

func ExceptTransientStatuses(excludeStatuses ...int) []int {
	excludes := map[int]struct{}{}
	for _, s := range excludeStatuses {
		excludes[s] = struct{}{}
	}

	var res []int
	for _, s := range TransientStatuses() {
		if _, ok := excludes[s]; !ok {
			res = append(res, s)
		}
	}
	return res
}

package requester_test

import (
	"errors"
	"fmt"
	"testing"

	"gitlab.com/gopkgz/transient/requester"
)

func TestStatusCodeError(t *testing.T) {
	testCases := []struct {
		name               string
		err                error
		expectedStatusCode int
	}{
		{"nil", nil, 0},
		{"not status code error", errors.New("not status code error"), 0},
		{"status code error", requester.HTTPStatusError{StatusCode: 500}, 500},
		{"status code error wrapped", fmt.Errorf("wrapped: %w", requester.HTTPStatusError{StatusCode: 500}), 500},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var sErr requester.HTTPStatusError
			if errors.As(tc.err, &sErr) {
				if sErr.StatusCode != tc.expectedStatusCode {
					t.Errorf("expected status code %d, got %d", tc.expectedStatusCode, sErr.StatusCode)
				}
			}
		})
	}
}

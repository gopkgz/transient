package requester

import "fmt"

type HTTPStatusError struct {
	URL        string
	StatusCode int
	Status     string
}

func (e HTTPStatusError) Error() string {
	return fmt.Sprintf("HTTP %d %s for %s", e.StatusCode, e.Status, e.URL)
}

func sliceToMap(s []int) map[int]struct{} {
	res := make(map[int]struct{})
	for _, v := range s {
		res[v] = struct{}{}
	}
	return res
}

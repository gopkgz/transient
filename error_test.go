package transient_test

import (
	"errors"
	"fmt"
	"testing"

	"gitlab.com/gopkgz/transient"
)

func TestTransientError(t *testing.T) {
	testCases := []struct {
		name              string
		err               error
		expectedTransient bool
	}{
		{"nil", nil, false},
		{"not transient", errors.New("not transient"), false},
		{"non-transient matching type", fakeTransientError{Err: errors.New("non-transient")}, false},
		{"transient", transient.Error{Err: errors.New("transient")}, true},
		{"transient wrapped", fmt.Errorf("wrapped: %w", transient.Error{Err: errors.New("transient")}), true},
		{"not transient wrapped", fmt.Errorf("wrapped: %w", errors.New("not transient")), false},
		{"not transient wrapped with transient", fmt.Errorf("wrapped: %w", transient.Error{Err: errors.New("not transient")}), true},
		{"not transient wrapped with transient wrapped", fmt.Errorf("wrapped: %w", fmt.Errorf("wrapped: %w", transient.Error{Err: errors.New("not transient")})), true},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if transient.IsTransientError(tc.err) != tc.expectedTransient {
				t.Errorf("expected transient %v, got %v", tc.expectedTransient, transient.IsTransientError(tc.err))
			}
		})
	}
}

type fakeTransientError struct {
	Err error
}

func (e fakeTransientError) Transient() bool {
	return false
}

func (e fakeTransientError) Error() string {
	return fmt.Sprintf("Fake transient error %s", e.Err)
}
